/**
 * Is basically an interface we'll use to build buttons with our factory.
 */
export class IAriaButtonComponent {
  /**
   * HTML id of the button this object will be handling aria features for.
   */
   public buttonId: string;

   public button: HTMLElement | undefined;

   public label: string;

   public details: string;

   public disabled: boolean;

   public color: string;

   public heldDown: boolean;

   public _onButtonDown: (($event: any) => void) | undefined;
   public _onButtonUp: (($event: any) => void) | undefined;
   public _onButtonClick: (($event: any) => void) | undefined;
   public _onButtonDblClick: (($event: any) => void) | undefined;
   public _getBoundedColor: (() => string) | undefined;
   public _getBoundedLabel: (() => string) | undefined;
   public _getBoundedDetails: (() => string) | undefined;
   public _getBoundedDisabled: (() => boolean) | undefined;
}
