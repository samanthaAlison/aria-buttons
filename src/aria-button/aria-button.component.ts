import { IAriaButtonComponent } from './aria-button-interface';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'aria-button',
  templateUrl: './aria-button.component.html',
  styleUrls: ['./aria-button.component.css'],
  providers: [
    IAriaButtonComponent
  ]
})
export class AriaButtonComponent implements OnInit {

  public static readonly ARIA_BUTTON_LISTENERS = {
    CLICK: 'click',
    DBLCLICK: 'dblclick',
    BUTTON_DOWN: 'down',
    BUTTON_UP: 'up'
  };

  public static setScreenReaderOn(on: boolean) {
    AriaButtonComponent.screenReaderOn = on;
  }

  private static screenReaderOn: boolean = false;

  /**
   * HTML id of the button this object will be handling aria features for.
   */
  private buttonId: string = '';

  private button: HTMLElement | undefined;

  private label: string = '';

  private details: string = '';

  private disabled: boolean = false;

  private color: string = '';

  private heldDown: boolean;

  private init: boolean = false;

  private _onButtonDown: (($event: any) => void) | undefined;
  private _onButtonUp: (($event: any) => void) | undefined;
  private _onButtonClick: (($event: any) => void) | undefined;
  private _onButtonDblClick: (($event: any) => void) | undefined;
  private _getBoundedColor: (() => string) | undefined;
  private _getBoundedLabel: (() => string) | undefined;
  private _getBoundedDetails: (() => string) | undefined;
  private _getBoundedDisabled: (() => boolean) | undefined;

  private facadeButton: Element | undefined = undefined;

  constructor(obj?: IAriaButtonComponent) {
    this.button = undefined;
    this._onButtonClick = obj._onButtonClick || undefined;
    this._onButtonDblClick = obj._onButtonDblClick || undefined;
    this._onButtonDown = obj._onButtonDown || undefined;
    this._onButtonUp = obj._onButtonUp || undefined;
    this._getBoundedColor = obj._getBoundedColor || undefined;
    this._getBoundedLabel = obj._getBoundedLabel || undefined;
    this._getBoundedDetails = obj._getBoundedDetails || undefined;
    this._getBoundedDisabled = obj._getBoundedDisabled || undefined;
    this.disabled = obj.disabled || false;
    this.bind(obj.buttonId || '');
    this.color = obj.color || '';
    this.details = obj.details || '';
  }

  public ngOnInit() {
    console.log('Aria button initialized');
    this.heldDown = false;
    this.init = true;
    this.button = document.getElementById(this.buttonId);
    if (this.button !== null) {
      this.facadeButton = this.button.getElementsByClassName('aria-button-facade')[0];
      const buttonClasses: DOMTokenList = this.button.classList;
      for (const d of Object.keys(buttonClasses)) {
        this.facadeButton.classList.add(d);
        this.button.classList.remove(d);

      }
    }

  }

  public bind(id: string) {
    this.buttonId = id;
    if (this.init) {
      this.button = document.getElementById(this.buttonId);
    }
  }

  public onButtonDown($event: any) {
    if (this._onButtonDown) {
      this._onButtonDown($event);
    }
    this.heldDown = true;
  }

  public onButtonUp($event: any) {
    if (this._onButtonUp) {
      this._onButtonUp($event);
    }
    this.heldDown = false;
  }

  public onButtonClick($event: any) {
    if (this.isScreenReaderOn() && this._onButtonUp && this._onButtonDown) {
      if (this.heldDown) {
        this.onButtonUp($event);
      } else {
        this.onButtonDown($event);
      }
    } else {
      if (this._onButtonClick) {
        this._onButtonClick($event);
      }
    }
  }

  public onButtonDblClick($event: any) {
    if (this._onButtonDblClick) {
      this._onButtonDblClick($event);
    }
  }

  public getColor(): string {
    if (this._getBoundedColor) {
      this.color = this._getBoundedColor();
    }
    return this.color;
  }

  public setColor(color: string): void {
    this.color = color;
    if (this.button) {
      this.button.style.color = this.color;
    }
  }
  public getDetails(): string {
    return this.details;
  }

  public isDisabled(): boolean {
    if (this._getBoundedDisabled) {
      this.disabled = this._getBoundedDisabled();
    }
    return this.disabled;
  }

  public getLabel(): string {
    if (this._getBoundedLabel) {
      this.label = this._getBoundedLabel();
    }
    return this.label;
  }

  public getId(): string {
    return this.buttonId;
  }

  public setAsHeldDown(heldDown: boolean) {
    this.heldDown = heldDown;
  }

  public addListenerCallback(listener: string, callback: ($event: any) => void, binding?: object) {
    if (binding) {
      callback = callback.bind(binding);
    }
    switch (listener) {
      case(AriaButtonComponent.ARIA_BUTTON_LISTENERS.BUTTON_DOWN):
        this._onButtonDown = callback;
        break;
      case(AriaButtonComponent.ARIA_BUTTON_LISTENERS.BUTTON_UP):
        this._onButtonUp = callback;
        break;
      case(AriaButtonComponent.ARIA_BUTTON_LISTENERS.CLICK):
        this._onButtonClick = callback;
        break;
      case(AriaButtonComponent.ARIA_BUTTON_LISTENERS.DBLCLICK):
        this._onButtonDblClick = callback;
        break;
      default:
        throw new Error(`Invalid listener name. You SHOULD be using the
         members of the ARIA_BUTTON_LISTENERS class to declare the listeners...`);
    }
  }

  protected isScreenReaderOn(): boolean {
    return AriaButtonComponent.screenReaderOn;
  }

}
