import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AriaButtonComponent } from './aria-button.component';
import { IAriaButtonComponent } from './aria-button-interface';

describe('AriaButtonComponent', () => {
  let component: AriaButtonComponent;
  let fixture: ComponentFixture<AriaButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AriaButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AriaButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
