import { IAriaButtonComponent } from './../aria-button/aria-button-interface';
import { AriaMultistateButtonState } from '../aria-multi-state-button-state/aria-multi-state-button-state';
export class IAriaMultiStateButtonComponent extends IAriaButtonComponent {
    public  statusMessageAriaLive: string;

    public states: AriaMultistateButtonState[] = [];

    public currentState: AriaMultistateButtonState | null;
}
