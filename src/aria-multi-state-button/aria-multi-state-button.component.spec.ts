import { IAriaMultiStateButtonComponent } from './aria-multi-state-button-interface';
import { AriaMultistateButtonState } from './../../tmp/src-inlined/aria-multi-state-button-state/aria-multi-state-button-state';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AriaMultiStateButtonComponent } from './aria-multi-state-button.component';
import { IAriaButtonComponent } from '..';

describe('AriaMultiStateButtonComponent', () => {
  let component: AriaMultiStateButtonComponent;
  let fixture: ComponentFixture<AriaMultiStateButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AriaMultiStateButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AriaMultiStateButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
