import { IAriaMultiStateButtonComponent } from './aria-multi-state-button-interface';
import { Component, OnInit } from '@angular/core';
import { AriaMultistateButtonState } from '../aria-multi-state-button-state/aria-multi-state-button-state';
import { AriaButtonComponent } from '../aria-button/aria-button.component';

@Component({
  selector: 'app-aria-multi-state-button',
  templateUrl: './aria-multi-state-button.component.html',
  styleUrls: ['./aria-multi-state-button.component.css'],
  providers: [
    IAriaMultiStateButtonComponent
  ]
})
export class AriaMultiStateButtonComponent extends AriaButtonComponent implements OnInit {

  public static readonly ARIA_LIVE_VALUES = Object.freeze({
    ASSERTIVE: 'assertive',
    OFF: 'off',
    POLITE: 'polite'
  });

  private statusMessageAriaLive: string;

  private states: AriaMultistateButtonState[] = [];

  private currentState: AriaMultistateButtonState | null;

  constructor(obj?: IAriaMultiStateButtonComponent) {
    super(obj);
    this.statusMessageAriaLive = obj.statusMessageAriaLive || AriaMultiStateButtonComponent.ARIA_LIVE_VALUES.POLITE;
    this.states = obj.states || [];
    this.currentState = obj.currentState || null;

   }

  public getStatusMessageAriaLive(): string {
    return this.statusMessageAriaLive;
  }

  public getMessage() {
    if (this.currentState !== null) {
      return this.currentState.statusMessage;
    }
  }

  public ngOnInit() {
    this.statusMessageAriaLive = AriaMultiStateButtonComponent.ARIA_LIVE_VALUES.POLITE;
  }

}
