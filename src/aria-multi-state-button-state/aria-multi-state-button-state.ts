export class AriaMultistateButtonState {
    constructor(
       public readonly id: string,
       public readonly statusMessage: string,
       public readonly color: string) {
    }
}
