import { IAriaButtonComponent } from './aria-button/aria-button-interface';
import { CommonModule } from '@angular/common';
import { AriaButtonComponent } from './aria-button/aria-button.component';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { AriaMultiStateButtonComponent } from './aria-multi-state-button/aria-multi-state-button.component';
import { AriaMultistateButtonState } from './aria-multi-state-button-state/aria-multi-state-button-state';
import { IAriaMultiStateButtonComponent } from './aria-multi-state-button/aria-multi-state-button-interface';
@NgModule({
    imports: [
      CommonModule
    ],
    declarations: [
      AriaButtonComponent,
      AriaMultiStateButtonComponent
    ],
    exports: [
      AriaButtonComponent,
      AriaMultiStateButtonComponent,
      CommonModule
    ],
    providers: [
      IAriaButtonComponent,
      IAriaMultiStateButtonComponent
    ]
  })

  export class AriaButtonModule {

    public static forRoot(): ModuleWithProviders {

        return {
          ngModule: AriaButtonModule
        };
    }
  }
