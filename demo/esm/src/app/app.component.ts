import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `<aria-button></aria-button>`
})
export class AppComponent {
  public header: string = 'UMD Demo';
}
